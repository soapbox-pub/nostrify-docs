# Integrations

Nostrify provides integrations with third-party libraries so you can use them seamlessly with Nostrify.

## Implementations

- [NDK](/integrations/ndk)