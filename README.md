# Nostrify Docs

Documentation for [Nostrify](https://gitlab.com/soapbox-pub/nostrify), built on [VitePress](https://vitepress.dev/).

## Local development

```sh
npm install
npm run dev
```